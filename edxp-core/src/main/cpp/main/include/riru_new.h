//
// Created by liumeng on 2020/10/14.
//

#ifndef EDXPOSED_RIRU_NEW_H
#define EDXPOSED_RIRU_NEW_H

#include <stdint.h>
#include <jni.h>
#include <sys/types.h>
typedef void(onModuleLoaded_v9)();

typedef int(shouldSkipUid_v9)(int uid);

typedef void(nativeForkAndSpecializePre_v9)(
        JNIEnv *env, jclass cls, jint *uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jintArray *fdsToClose, jintArray *fdsToIgnore, jboolean *is_child_zygote,
        jstring *instructionSet, jstring *appDataDir, jboolean *isTopApp, jobjectArray *pkgDataInfoList,
        jobjectArray *whitelistedDataInfoList, jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs);

typedef int(nativeForkAndSpecializePost_v9)(JNIEnv *env, jclass cls, jint res);

typedef void(nativeForkSystemServerPre_v9)(
        JNIEnv *env, jclass cls, uid_t *uid, gid_t *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jlong *permittedCapabilities, jlong *effectiveCapabilities);

typedef int(nativeForkSystemServerPost_v9)(JNIEnv *env, jclass cls, jint res);

typedef void(nativeSpecializeAppProcessPre_v9)(
        JNIEnv *env, jclass cls, jint *uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jboolean *startChildZygote, jstring *instructionSet, jstring *appDataDir,
        jboolean *isTopApp, jobjectArray *pkgDataInfoList, jobjectArray *whitelistedDataInfoList,
        jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs);

typedef int(nativeSpecializeAppProcessPost_v9)(JNIEnv *env, jclass cls);

typedef struct {
    int supportHide;
    int version;
    const char *versionName;
    onModuleLoaded_v9 *onModuleLoaded;
    shouldSkipUid_v9 *shouldSkipUid;
    nativeForkAndSpecializePre_v9 *forkAndSpecializePre;
    nativeForkAndSpecializePost_v9 *forkAndSpecializePost;
    nativeForkSystemServerPre_v9 *forkSystemServerPre;
    nativeForkSystemServerPost_v9 *forkSystemServerPost;
    nativeSpecializeAppProcessPre_v9 *specializeAppProcessPre;
    nativeSpecializeAppProcessPost_v9 *specializeAppProcessPost;
} RiruModuleInfoV9;

// ---------------------------------------------------------

typedef void *(RiruGetFunc_v9)(uint32_t token, const char *name);

typedef void (RiruSetFunc_v9)(uint32_t token, const char *name, void *func);

typedef void *(RiruGetJNINativeMethodFunc_v9)(uint32_t token, const char *className, const char *name, const char *signature);

typedef void (RiruSetJNINativeMethodFunc_v9)(uint32_t token, const char *className, const char *name, const char *signature, void *func);

typedef const JNINativeMethod *(RiruGetOriginalJNINativeMethodFunc_v9)(const char *className, const char *name, const char *signature);

typedef struct {

    uint32_t token;
    RiruGetFunc_v9 *getFunc;
    RiruGetJNINativeMethodFunc_v9 *getJNINativeMethodFunc;
    RiruSetFunc_v9 *setFunc;
    RiruSetJNINativeMethodFunc_v9 *setJNINativeMethodFunc;
    RiruGetOriginalJNINativeMethodFunc_v9 *getOriginalJNINativeMethodFunc;
} RiruApiV9;



#endif //EDXPOSED_RIRU_NEW_H
