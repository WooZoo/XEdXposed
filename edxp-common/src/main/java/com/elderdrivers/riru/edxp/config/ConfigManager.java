package com.elderdrivers.riru.edxp.config;

public class ConfigManager {

    public static String appDataDir = "";
    public static String niceName = "";
    public static String appProcessName = "";



    public static native boolean isBlackWhiteListEnabled();

    public static native boolean isDynamicModulesEnabled();

    public static native boolean isResourcesHookEnabled();

    public static native boolean isDeoptBootImageEnabled();

    public static native String getInstallerPackageName();

    public static native boolean isAppNeedHook(String appDataDir);
}
